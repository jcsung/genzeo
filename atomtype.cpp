#include "atomtype.h"
#include <string>
#include <iostream>
#include <iomanip>

using namespace std;

ostream &operator<<(ostream &os, const atomtype &at){
	os<<setiosflags(ios_base::right|ios_base::fixed);
	os<<setw(3)<<at.index;
	os<<setw(2)<<at.type;
	os<<setw(11)<<setprecision(1)<<at.epsilon;
	os<<setw(11)<<setprecision(2)<<at.sigma;
	os<<setw(11)<<setprecision(3)<<at.charge;
	os<<setw(11)<<setprecision(4)<<at.mass;
	os<<setw(4)<<at.chemid;
	os<<" "<<at.chname;
	os<<"\t"<<at.count;
	return os;
}

istream &operator>>(istream &is,atomtype &at){
	is>>at.index>>at.type>>at.epsilon>>at.sigma>>at.charge>>at.mass>>at.chemid>>at.chname;
	return is;
}

bool operator==(atomtype &a1, atomtype &a2){
	return a1.index==a2.index&&a1.chname==a2.chname;
}
