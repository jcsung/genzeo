#ifndef RDF_H
#define RDF_H

#include <vector>
#include <string>
#include "atom.h"
#include "xtalpdbdata.h"
#include "rdf.h"

using namespace std;

class rdf{
	public:
		//Constructor
		rdf(int nbin, const vector <int> &thebins, const vector <int> &nintgrl, const vector <double> &rdfn, double min, double max, double dif);

		vector <double> get_bin(int index) const;
		vector <double> get_number_integral(int index) const;
		vector <double> get_rdf(int index) const;

		string print_bins() const;
		string print_number_integral() const;
		string print_rdf() const;

	private:
		int numbin;
		vector <int> mybins;
		vector <int> nint;
		vector <double> radiald;
		double rmin;
		double rmax;
		double dr;
		int maxbin;
};
#endif
