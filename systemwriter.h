#ifndef SYSTEMWRITER_H
#define SYSTEMWRITER_H
#include <vector>
#include <string>
#include <fstream>
#include "chemicalsystem.h"
class SystemWriter{
	public:
		SystemWriter();
		virtual void write(const string &fout, chemicalsystem &sys);
		virtual void write(const string &fout, chemicalsystem &sys, bool ONEFILE) = 0;
		virtual void write(ofstream &fout, Frame &f, bool ONEFILE) = 0;
};
#endif
