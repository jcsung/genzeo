#ifndef MCCCS_MOVIE_READER_H
#define MCCCS_MOVIE_READER_H

#include "systemreader.h"
#include "chemicalsystem.h"
class MCCCSMovieReader : public SystemReader
{
	public:
		MCCCSMovieReader();
		virtual chemicalsystem read(const string &fin);
};
#endif
