#ifndef UTIL_H
#define UTIL_H

#include <string>
#include <vector>

using namespace std;

namespace util{
	string int2str(int val);
	int str2int(const string &s);
	string double2str(double d);
	double str2double(const string &s);
	double pi();
	bool dequals(double a, double b);
	bool dequals(double a, double b, double tol);
	double deg2rad(double ang);
	double rad2deg(double ang);
	string trim(const string &s);
	vector <string> splitter(const string &str);
}
#endif
