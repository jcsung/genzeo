#ifndef FORCEFIELD_H
#define FORCEFIELD_H

#include <fstream>
#include <vector>
#include <string>
#include "atomtype.h"

using namespace std;

class ForceField{
	public:
		//Constructor
		ForceField();

		//Accessors
		vector <atomtype> getTypes() const;

		//Modifiers
		void read(const string &fin);

		//Output
		friend ostream& operator<<(ostream &os, const ForceField &f);

	private:
		//Main stuff
		vector <atomtype> atomtypes;

};
#endif
