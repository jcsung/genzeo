#include "xyzwriter.h"

XYZWriter::XYZWriter(){

}

void XYZWriter::write(const string &fout, chemicalsystem &sys, bool ONEFILE){
	if (ONEFILE) cerr<<"multi-file XYZ writing not yet implemented"<<endl;
	Frame f;
	ofstream outfile(fout.c_str());
	for (int x=0; x<sys.size(); x++){
		f=sys.get(x+1);
		write(outfile,f,ONEFILE);
	}
	outfile.close();	
}

void XYZWriter::write(ofstream &fout, Frame &f, bool ONEFILE){
	int x;
	atom at;
	fout<<f.size()<<endl;
	fout<<endl;
	for (x=0; x<f.size(); x++){
		at=f.get(x+1);
		fout<<at.name<<" "<<at.x<<" "<<at.y<<" "<<at.z<<endl;
	}
}

