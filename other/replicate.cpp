#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <vector>
#include "util.h"
#include "frame.h"
#include "xtalpdbdata.h"
#include <cmath>
#include <sstream>

using namespace std;
using namespace util;

vector <string> split(const string &str);
atom readatom(const string &str2);

atom readatom(const string &str2){
	string str=str2;
	while (str.size()<80) str+=" ";
	atom at;
	at.record=trim(str.substr(0,6));
	at.serial=str2int(trim(str.substr(6,5)));
	at.name=trim(str.substr(12,4));
	at.altLoc=str[16];
	at.resName=trim(str.substr(17,3));
	at.chainID=str[21];
	at.resSeq=str2int(trim(str.substr(22,4)));
	at.iCode=str[26];
	at.x=str2double(trim(str.substr(30,8)));
	at.y=str2double(trim(str.substr(38,8)));
	at.z=str2double(trim(str.substr(46,8)));
	at.occupancy=str2double(trim(str.substr(54,6)));
	at.tempFactor=str2double(trim(str.substr(60,6)));
	at.element=trim(str.substr(76,2));
	at.charge=trim(str.substr(78,2));
	return at;
}

vector <string> split(const string &str){
	vector <string> ret(1);
	stringstream ss(str);
	string str2;
	int x=0;
	while (ss>>str2){
//		cout<<"String="<<str2<<endl;
		ret[x++]=str2;
		if (x==ret.size()) ret.resize(2*x);
	}
	ret.resize(x);
	return ret;
}

int main(int argc,char *argv[]){

	if (argc-1<2){
		cout<<"Syntax: "<<argv[0]<<" FILE_IN FILE_OUT"<<endl;
	}
	else{
		//1. Read the system
		string junk;
		xtalpdbdata dat;
		Frame f;
		atom at;
		ifstream infile(argv[1]);
		infile>>junk>>dat.a>>dat.b>>dat.c>>dat.alpha>>dat.beta>>dat.gamma>>dat.sGroup>>dat.z;
		f.updateXtal(dat);
		infile.ignore(100,'\n');
		while (getline(infile,junk)){
			at=readatom(junk);
			f.add(at);
		}	
		infile.close();	
		//cout<<f.size()<<endl;
		cout<<"Cell Vectors: "<<endl;
		cout<<f.print_cellVectors()<<endl;
		cout<<"Changing"<<endl;
		vector <double> vec=f.cellVectors();
		vec[7]=-vec[7];

		f.updateXtal(vec);
		cout<<"New cell vectors: "<<endl;
		cout<<f.print_cellVectors()<<endl;
		
		int num=f.size();
//		int count=num+1;
		int count=1;
/*
		for (int y=1; y<=3; y++){
			for (int x=1; x<=num; x++){
				at=f.get(x);
				at.serial=count++;
				at.resSeq=y+1;
				f.add(at);

			}	
		}
*/
		cout<<f.print_cellVectors()<<endl;
		int x, y, z, w;
		int resnum=1;
		Frame g;
		for (x=0; x<=3; x++){
			for (y=0; y<=1; y++){
				for (z=0; z<=1; z++){
					cout<<"x="<<x<<" y="<<y<<" z="<<z<<endl;
					for (w=1; w<=num; w++){
						at=f.get(w);
						at.serial=count++;
						at.resSeq=resnum;
						at.x+=x*vec[0]+y*vec[3]+z*vec[6];
						at.y+=x*vec[1]+y*vec[4]+z*vec[7];
						at.z+=x*vec[2]+y*vec[5]+z*vec[8];
						g.add(at);
					}
					resnum++;
				}
			}
		}
		dat.a*=4;
		dat.b*=2;
		dat.c*=2;

		g.updateXtal(dat);

		//3. Write the output
		ofstream outfile(argv[2]);
		outfile<<g;//<<endl;
		outfile.close();
	}
	return 0;
}
