#include <cmath>
#include <iostream>
#include "atom.h"
#include "util.h"
#include "xtalpdbdata.h"
#include "frame.h"
#include <fstream>
#include <string>
//#include "rdf.h"
using namespace std;
using namespace util;

atom readatom(const string &str);

atom readatom(const string &str2){
	string str=str2;
	while (str.size()<80) str+=" ";
	atom at;
        at.record=trim(str.substr(0,6));
	at.serial=str2int(trim(str.substr(6,5)));
	at.name=trim(str.substr(12,4));
	at.altLoc=str[16];
	at.resName=trim(str.substr(17,3));
	at.chainID=str[21];
	at.resSeq=str2int(trim(str.substr(22,4)));
	at.iCode=str[26];
	at.x=str2double(trim(str.substr(30,8)));
	at.y=str2double(trim(str.substr(38,8)));
	at.z=str2double(trim(str.substr(46,8)));
	at.occupancy=str2double(trim(str.substr(54,6)));
	at.tempFactor=str2double(trim(str.substr(60,6)));
	at.element=trim(str.substr(76,2));
	at.charge=trim(str.substr(78,2));
	return at;
}

int main(){
	string junk;
	xtalpdbdata dat;
	Frame f;
	atom at;
	ifstream infile("3.pdb");
	//xtal data
	infile>>junk>>dat.a>>dat.b>>dat.c>>dat.alpha>>dat.beta>>dat.gamma>>dat.sGroup>>dat.z;
	cout<<"Before update: "<<f.print_cellVectors()<<endl;
	f.updateXtal(dat);
	cout<<"After update: "<<f.print_cellVectors()<<endl;
	//ATOM      1  CD  UNK     1       6.755   4.052  -0.203  1.00  0.89           C

	return 0;
}
