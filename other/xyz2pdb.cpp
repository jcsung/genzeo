#include <iostream>
#include <fstream>
#include <string>
#include "util.h"
#include "frame.h"
#include "chemicalsystem.h"

using namespace std;
using namespace util;

int main(int argc,char *argv[]){

	if (argc-1<2){
		cout<<"Syntax: "<<argv[0]<<" XYZ_IN PDB_OUT"<<endl;
	}
	else{
		int num, i, j=1;
		string junk;
		Frame f;
		atom a;
		a.record="ATOM";
		a.altLoc=' ';
		a.resName="H2S";
		a.chainID=' ';
		a.iCode=' ';
		a.occupancy=0;
		a.tempFactor=0;
		a.charge="";
		ifstream infile(argv[1]);
		ofstream outfile(argv[2]);
		infile>>num;
		infile.ignore(100,'\n');
		getline(infile,junk); //read title junk
		for (i=1; i<=num; i++){
			infile>>a.name>>a.x>>a.y>>a.z;
			a.serial=i;
			a.resSeq=j;
			if (i%4==0) j++;
			f.add(a);
		}
		infile.close();
		outfile<<f;
		outfile.close();
	}
	return 0;
}
