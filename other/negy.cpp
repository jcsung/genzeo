#include <iostream>
#include <fstream>
#include <string>
#include "util.h"
#include "frame.h"
#include "chemicalsystem.h"
#include "xtalpdbdata.h"
#include <vector>
#include <iomanip>

using namespace std;
using namespace util;


struct atomtype{
	string name;
	int id;
	double radius;
};

atom readatom(const string &str2);

atom readatom(const string &str2){
	string str=str2;
	while (str.size()<80) str+=" ";
	atom at;
	at.record=trim(str.substr(0,6));
	at.serial=str2int(trim(str.substr(6,5)));
	at.name=trim(str.substr(12,4));
	at.altLoc=str[16];
	at.resName=trim(str.substr(17,3));
	at.chainID=str[21];
	at.resSeq=str2int(trim(str.substr(22,4)));
	at.iCode=str[26];
	at.x=str2double(trim(str.substr(30,8)));
	at.y=str2double(trim(str.substr(38,8)));
	at.z=str2double(trim(str.substr(46,8)));
	at.occupancy=str2double(trim(str.substr(54,6)));
	at.tempFactor=str2double(trim(str.substr(60,6)));
	at.element=trim(str.substr(76,2));
	at.charge=trim(str.substr(78,2));
	return at;
}

int main(int argc,char *argv[]){

	if (argc-1<5){
		cout<<"Syntax: "<<argv[0]<<" PDB_IN CSSR_OUT NUM_X_UNITCELL NUM_Y_UNITCELL NUM_Z_UNITCELL"<<endl;
	}
	else{
		string junk;
		Frame f;
		atom at;
		xtalpdbdata dat;
		vector <atomtype> alltypes(1);
		vector <string> mytypes(1);

		//1. Read PDB
		ifstream infile(argv[1]);
		infile>>junk>>dat.a>>dat.b>>dat.c>>dat.alpha>>dat.beta>>dat.gamma>>dat.sGroup>>dat.z;
		f.updateXtal(dat);
		infile.ignore(100,'\n');
		while (getline(infile,junk)){
			at=readatom(junk);
			f.add(at);
		}
		infile.close();

		//2. Read all possible types
		int x=0, y;
		infile.open("types.dat");
		while (infile>>alltypes[x].name>>alltypes[x].id>>alltypes[x].radius){
			x++;
			if (x==alltypes.size()) alltypes.resize(2*x);
		}
		alltypes.resize(x);
		infile.close();

		//3. Determine number of types
		bool flag;
		mytypes[0]=f.get(1).name;
		for (x=2; x<=f.size(); x++){
			flag=true;
			for (y=0; y<mytypes.size(); y++){
				if (mytypes[y]==f.get(x).name){
					flag=false;
					break;
				}
			}
			if (flag){
				mytypes.resize(mytypes.size()+1);
				mytypes[mytypes.size()-1]=f.get(x).name;
			}
		}
		


		ofstream outfile(argv[2]);
		outfile<<setiosflags(ios_base::right|ios_base::fixed);
		outfile<<setw(9)<<setprecision(3)<<dat.a;
		outfile<<setw(9)<<setprecision(3)<<dat.b;
		outfile<<setw(9)<<setprecision(3)<<dat.c;
		outfile<<setw(7)<<setprecision(2)<<dat.alpha;
		outfile<<setw(7)<<setprecision(2)<<dat.beta;
		outfile<<setw(7)<<setprecision(2)<<dat.gamma;
		outfile<<" ";
		outfile<<argv[3]<<" "<<argv[4]<<" "<<argv[5]<<endl;
		outfile<<f.size()<<" "<<mytypes.size()<<endl;
		for (x=0; x<mytypes.size(); x++){
			for (y=0; y<alltypes.size(); y++){
//				cout<<"checking against type "<<alltypes[y].name<<endl;
				flag=false;
				if (mytypes[x]==alltypes[y].name){
					flag=true;
					break;	
				}
			}
			if (flag){
				outfile<<alltypes[y].name<<" "<<alltypes[y].id<<" "<<alltypes[y].radius<<endl;
			}
			else{
				cout<<"ERROR: Type "<<mytypes[x]<<" not found in type database"<<endl;
			}
		}
		//$str=sprintf("%5d%5s  %10.5f%10.5f%10.5f%8.3f",$line[1],$line[2],$line[5],$line[6],$line[7],0);
		for (x=1; x<=f.size(); x++){
			at=f.get(x);
			outfile<<setw(5)<<at.serial;
			outfile<<setw(5)<<at.name;
			outfile<<"  ";
			outfile<<setw(10)<<setprecision(5)<<at.x;
			outfile<<setw(10)<<setprecision(5)<<-at.y;
			outfile<<setw(10)<<setprecision(5)<<at.z;
			outfile<<setw(8)<<setprecision(3)<<0.0; //charge lol
			outfile<<endl;
		}
		outfile.close();

	}
	return 0;
}
