#include <iostream>
#include <fstream>
#include <string>
#include "util.h"
#include "frame.h"
#include "chemicalsystem.h"

using namespace std;
using namespace util;

int main(int argc,char *argv[]){

	if (argc-1<2){
		cout<<"Syntax: "<<argv[0]<<" XYZ_IN PDB_OUT"<<endl;
	}
	else{
		int num, i, j=1;
		string junk;
		Frame f;
		atom a;
		chemicalsystem sys;
		ifstream infile(argv[1]);
		while (infile>>num){
			j=1;
			infile.ignore(100,'\n');
			getline(infile,junk); //read title junk

			a.record="ATOM";
			a.altLoc=' ';
			a.resName="ENE";
			a.chainID=' ';
			a.iCode=' ';
			a.occupancy=0;
			a.tempFactor=0;
			a.charge="";
	
			for (i=1; i<=num; i++){
				infile>>a.name>>a.x>>a.y>>a.z;
				a.serial=i;
				a.resSeq=j;
				if (i%5==0) j++;
				f.add(a);
			}
			sys.add(f);
			f.clear();
		}
		infile.close();

		sys.generate_data();		
		sys.equalize_atom_count();

		ofstream outfile(argv[2]);
		outfile<<sys;
		outfile.close();
	}
	return 0;
}
