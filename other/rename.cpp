#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <vector>
#include "util.h"
#include "frame.h"
#include "xtalpdbdata.h"
#include <cmath>
#include <sstream>

using namespace std;
using namespace util;

vector <string> split(const string &str);
atom readatom(const string &str2);

atom readatom(const string &str2){
	string str=str2;
	while (str.size()<80) str+=" ";
	atom at;
	at.record=trim(str.substr(0,6));
	at.serial=str2int(trim(str.substr(6,5)));
	at.name=trim(str.substr(12,4));
	at.altLoc=str[16];
	at.resName=trim(str.substr(17,3));
	at.chainID=str[21];
	at.resSeq=str2int(trim(str.substr(22,4)));
	at.iCode=str[26];
	at.x=str2double(trim(str.substr(30,8)));
	at.y=str2double(trim(str.substr(38,8)));
	at.z=str2double(trim(str.substr(46,8)));
	at.occupancy=str2double(trim(str.substr(54,6)));
	at.tempFactor=str2double(trim(str.substr(60,6)));
	at.element=trim(str.substr(76,2));
	at.charge=trim(str.substr(78,2));
	return at;
}

vector <string> split(const string &str){
	vector <string> ret(1);
	stringstream ss(str);
	string str2;
	int x=0;
	while (ss>>str2){
//		cout<<"String="<<str2<<endl;
		ret[x++]=str2;
		if (x==ret.size()) ret.resize(2*x);
	}
	ret.resize(x);
	return ret;
}

int main(int argc,char *argv[]){

	if (argc-1<2){
		cout<<"Syntax: "<<argv[0]<<" FILE_IN FILE_OUT"<<endl;
	}
	else{
		//1. Read the system
		string junk;
		xtalpdbdata dat;
		Frame f;
		atom at;
		ifstream infile(argv[1]);
		infile>>junk>>dat.a>>dat.b>>dat.c>>dat.alpha>>dat.beta>>dat.gamma>>dat.sGroup>>dat.z;
		f.updateXtal(dat);
		infile.ignore(100,'\n');
		while (getline(infile,junk)){
			at=readatom(junk);
			f.add(at);
		}	
		infile.close();	
		//cout<<f.size()<<endl;
		//2. Read the things to change and change them
		vector <string> sep;
		string name;
		int x, index;
		infile.open("whichchange");
		while (getline(infile,junk)){
			//Split line
			sep=split(junk);
			name=sep[0];
			for (x=1; x<sep.size(); x++){
				//cout<<x<<"/"<<sep.size()-1<<endl;
				index=str2int(sep[x])+1;
				//cout<<"Index is: "<<index<<endl;
				//f.get(index).name=name;
				at=f.get(index);
				at.name=name;
				f.set(index,at);
				//cout<<"Corresponding Atom Name: "<<f.get(index).name<<endl;
			}
//			cout<<"TOCHANGE: "<<name<<endl;
		}
		infile.close();
		//3. Write the output
		ofstream outfile(argv[2]);
		outfile<<f<<endl;
		outfile.close();
	}
	return 0;
}
