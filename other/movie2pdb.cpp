#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <vector>
#include "util.h"
#include "frame.h"
#include "chemicalsystem.h"
#include <cmath>

using namespace std;
using namespace util;

struct ljpoten{
	int itype;
	double sigma;
	double epsilon;
	double q;
	double mass;
	string chemid;
	string chname;
	friend ostream &operator<<(ostream &os,const ljpoten &at);
};

ostream &operator<<(ostream &os, const ljpoten &at){
	os<<at.itype<<" "<<at.sigma<<" "<<at.epsilon<<" "<<at.q<<" "<<at.mass<<" "<<at.chemid<<" "<<at.chname;
	return os;
}

ljpoten parse(const string &str);
string getName(int beadtype, const vector <ljpoten> &types);

string getName(int beadtype, const vector <ljpoten> &types){
	string str="X";
	int x;
	for (x=0; x<types.size(); x++){
		if (types[x].itype==beadtype){
			str=types[x].chemid;
			break;
		}
	}
	return str;
}

ljpoten parse(const string &str){
	ljpoten pot;
	int x, count=0;
	char cur='a';
	char prev=(char)13;

//	Remove extra spaces
	string fixed="";	
	for (x=0; x<str.size(); x++){
		cur=str[x];
		if (cur!=' '){
			fixed+=cur;
		}
		else if (prev!=' '){
			fixed+=cur;
			count++;
		}
		prev=cur;
	}
//	cout<<"Before: "<<str<<" After: "<<fixed<<endl;
//	cout<<"count="<<count<<endl;
//	Split
	count++; //because there should be one less space than words
//	cout<<"fixed count: "<<count<<endl;
	vector <string> splitted(count,"");
	int y=0;
	for (x=0; x<fixed.size(); x++){
		cur=fixed[x];
		if (cur!=' '){
			splitted[y]+=cur;
		}
		else{
//			cout<<"splitted["<<y<<"]="<<splitted[y]<<endl;
			y++;
		}
	}	
//	cout<<"splitted["<<count-1<<"]="<<splitted[count-1]<<endl;
	pot.itype=str2int(splitted[0]);
	pot.sigma=str2double(splitted[1]);
	pot.epsilon=str2double(splitted[2]);
	pot.q=str2double(splitted[3]);
	pot.mass=str2double(splitted[4]);
	pot.chemid=splitted[5];
	pot.chname="";
	if (count>6){
		for(x=6; x<count; x++) pot.chname+=splitted[x]+" ";
	}
	return pot;
}

int main(int argc,char *argv[]){

	if (argc-1<2){
		cout<<"Syntax: "<<argv[0]<<" MOVIE.DAT_FILENAME OUTPUT_PDB_PREFIX"<<endl;
	}
	else{

		string outfix=argv[2];

//		1. Read types from lj.poten
		string str;
		int numtype;

//		Open file
		ifstream infile("lj.poten");
//		Read junk
		getline(infile,str);
//		Number of types
		infile>>numtype;
		infile.ignore(100,'\n'); //get newline
//		Read junk
		getline(infile,str);
		int x;
//		Get the different types
		vector <ljpoten> types(numtype);	
		for (x=0; x<numtype; x++){
			getline(infile,str); //read line
			types[x]=parse(str);
			cout<<types[x]<<endl;
		}
//		Close file
		infile.close();

//		2. Read the movie file
		int nframes, nmolec;
		int ntype, nbox, nbeadtype;
		vector <double> rcut;
		vector <int> usedtypes;
		infile.open(argv[1]);
		infile>>nframes>>nmolec>>ntype>>nbox>>nbeadtype;
		cout<<"nmolec="<<nmolec<<endl;
		rcut.resize(nbox);
		for (x=0; x<nbox; x++) infile>>rcut[x];	
		usedtypes.resize(nbeadtype);
		for (x=0; x<nbeadtype; x++) infile>>usedtypes[x];
//		Read connectivity
//		This is not needed for the conversion process
		int count, counted_data, bead_of_type;
		int y, z;
		for (x=0; x<ntype; x++){
//			Read #beads in molec $x
			infile>>bead_of_type;
			cout<<"Beads of type "<<x<<": "<<bead_of_type<<endl;
			for (y=0; y<bead_of_type; y++){
//				Read #bonds for bead $y
				infile>>count;
				for (z=0; z<count; z++){
//					Read bond $z
					infile>>counted_data;
				}
			}
			for (y=0; y<bead_of_type; y++){
//				Read #torsisons for bead $y
				infile>>count;
				for (z=0; z<count; z++){
//					Read torsion $z
					infile>>counted_data;
				}
			}
		}
//		Now read all the frames
		int lcv;
		int ncycle;
		vector <double> cur_cell(9);
		vector <Frame> cur_frame(nbox);
		vector <chemicalsystem> sys(nbox);
		atom cur_atom;

		int molid;
		int moltyp;
		int nbeadmolec;
		int whichbox;
		vector <int> serial(nbox);
		vector <int> resSeq(nbox);
		int cur_type;
		double junk;

		cout<<"Begin frame read"<<endl;
		for (lcv=0; lcv<nframes; lcv++){
			cout<<"Reading Frame #"<<lcv+1<<"/"<<nframes<<endl;
//			Read number of cycles
			infile>>ncycle;
			cout<<"ncycle="<<ncycle<<endl;
//			Reset the serial/resSeq
			for (x=0; x<nbox; x++){
				serial[x]=1;
				resSeq[x]=1;
			}
//			Read number of molecules of each type in each box
			for (x=0; x<nbox; x++){
				cout<<"nbox="<<x<<endl;
				cout<<outfix+int2str(x)+".pdb"<<endl;
				for (y=0; y<ntype; y++){
//					Read number of molecules of type $y in box $x
					infile>>counted_data; //Just read to hear because don't need it
					cout<<"y="<<y<<" Type read in: "<<counted_data<<endl;
				}
//				Read in the dimensions
				for (y=0; y<cur_cell.size(); y++){
				/*	if (x==0){
						infile>>cur_cell[y];
					}
					else*/ if (y==0||y==4||y==8){
						infile>>cur_cell[y];
					}
					else{
						cur_cell[y]=0;
					}
					cout<<"cur_cell["<<y<<"]="<<cur_cell[y]<<endl;
				}	
				cur_frame[x].updateXtal(cur_cell);
				cout<<cur_frame[x].print_cellVectors()<<endl;
			}

//			Now read in the molecules
			for (x=0; x<nmolec; x++){
//				Read in molecule data
				infile>>molid>>moltyp>>nbeadmolec>>whichbox;				
//				Read in COM data (useless here)
				infile>>junk>>junk>>junk;
				for (y=0; y<nbeadmolec; y++){
//					Read in the coordinates of the bead	
					infile>>cur_atom.x>>cur_atom.y>>cur_atom.z;
//					Read in the partial charge

//					It's not needed
					infile>>junk;
//					cur_atom.charge=double2str(junk);
//					Read in the bead type
					infile>>cur_type;
//					Get what the name is
					cur_atom.name=getName(cur_type,types);
//					Fill in the remaining junk
					cur_atom.record="ATOM";
					cur_atom.serial=serial[whichbox-1]++;
					cur_atom.altLoc=' ';
					cur_atom.resName="MOL";
					cur_atom.chainID=' ';
					cur_atom.resSeq=resSeq[whichbox-1];
					cur_atom.iCode=' ';
					cur_atom.occupancy=1;
					cur_atom.tempFactor=1;
					cur_atom.element=" ";
//					cout<<cur_atom<<endl;
//					Add it to the relevant frame
					cur_frame[whichbox-1].add(cur_atom);
				}
				resSeq[whichbox-1]++;
//				break;
			}
//			Add the frames to the system
			for (x=0; x<nbox; x++){
				sys[x].add(cur_frame[x]);
				cur_frame[x].clear();
			}
						
		}
		infile.close();
		cout<<"Frame read complete"<<endl;

//		Output	
		cout<<"Begin output"<<endl;
		ofstream outfile[nbox];
		string curname;
		for (x=0; x<nbox; x++){
			curname=outfix+int2str(x+1)+".pdb";
			outfile[x].open(curname.c_str());
			outfile[x]<<sys[x];
			outfile[x].close();
		}
		cout<<"Output complete"<<endl;

		cout<<"Begin VMD-compatible output generation"<<endl;
		for (x=0; x<nbox; x++){
			sys[x].generate_data();
			sys[x].equalize_atom_count();
			curname="vmd"+outfix+int2str(x+1)+".pdb";
			outfile[x].open(curname.c_str());
			outfile[x]<<sys[x];
			outfile[x].close();
		}	
		cout<<"VMD-compatible output complete"<<endl;

		cout<<"Done"<<endl;

	}
	return 0;
}
