#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <vector>
#include "util.h"
#include "frame.h"
#include "xtalpdbdata.h"
#include <cmath>
#include <sstream>

using namespace std;
using namespace util;

vector <string> split(const string &str);
atom readatom(const string &str2);

atom readatom(const string &str2){
	string str=str2;
	while (str.size()<80) str+=" ";
	atom at;
	at.record=trim(str.substr(0,6));
	at.serial=str2int(trim(str.substr(6,5)));
	at.name=trim(str.substr(12,4));
	at.altLoc=str[16];
	at.resName=trim(str.substr(17,3));
	at.chainID=str[21];
	at.resSeq=str2int(trim(str.substr(22,4)));
	at.iCode=str[26];
	at.x=str2double(trim(str.substr(30,8)));
	at.y=str2double(trim(str.substr(38,8)));
	at.z=str2double(trim(str.substr(46,8)));
	at.occupancy=str2double(trim(str.substr(54,6)));
	at.tempFactor=str2double(trim(str.substr(60,6)));
	at.element=trim(str.substr(76,2));
	at.charge=trim(str.substr(78,2));

	if (at.element=="C") at.mass=12.011;
	else if (at.element=="O") at.mass=16;
	else if (at.element=="FE") at.mass=55.85;
	else if (at.element=="H") at.mass=1.008;

	return at;
}

vector <string> split(const string &str){
	vector <string> ret(1);
	stringstream ss(str);
	string str2;
	int x=0;
	while (ss>>str2){
//		cout<<"String="<<str2<<endl;
		ret[x++]=str2;
		if (x==ret.size()) ret.resize(2*x);
	}
	ret.resize(x);
	return ret;
}

int main(int argc,char *argv[]){

	if (argc-1<2){
		cout<<"Syntax: "<<argv[0]<<" FILE_IN FILE_OUT"<<endl;
	}
	else{
		//1. Read the system
		string junk;
		xtalpdbdata dat;
		Frame f;
		atom at;
		ifstream infile(argv[1]);
		infile>>junk>>dat.a>>dat.b>>dat.c>>dat.alpha>>dat.beta>>dat.gamma>>dat.sGroup>>dat.z;
		f.updateXtal(dat);
		infile.ignore(100,'\n');
		while (getline(infile,junk)){
			at=readatom(junk);
			f.add(at);
		}	
		infile.close();	
		//cout<<f.size()<<endl;
		vector <double> vec=f.cellVectors();
		int num=f.size();
		cout<<f.print_cellVectors()<<endl;
	
		Frame g;
		g.updateXtal(dat);
		int x, y, z, w;
		int count=1;
		int resSeq=1;
		for (x=0; x<=3; x++){
			for (y=1; y<=num; y++){
				at=f.get(y);
				at.resSeq=count++;
				at.resSeq=resSeq;
				at.x+=x*vec[0];
				at.y+=x*vec[1];
				at.z+=x*vec[2];
				g.add(at);
			}
			resSeq=2;
		}
		num=g.size();

		for (x=1; x<=num; x++){
			at=g.get(x);
			at.x+=vec[3];
			at.y+=vec[4];
			at.z+=vec[5];
			g.add(at);
		}
		num=g.size();

		for (x=1; x<=num; x++){
			at=g.get(x);
			at.x+=vec[6];
			at.y+=vec[7];
			at.z+=vec[8];
			g.add(at);
		}


		//3. Write the output
		ofstream outfile(argv[2]);
		outfile<<g;//<<endl;
		outfile.close();

		cout<<g.com()<<endl;

	}
	return 0;
}
