#ifndef CHEMICALSYSTEM_H
#define CHEMICALSYSTEM_H

#include <vector>
#include <string>
#include "atom.h"
#include "xtalpdbdata.h"
#include "frame.h"
#include "rdf.h"
#include <sstream>

using namespace std;

struct histogram{
	string name;
	int count;
};

class chemicalsystem{
	public:
		//Constructor
		chemicalsystem();

		//Accessors
		int size() const;
		Frame get(int index) const;

		//Modifiers
		bool set(int index, const Frame &nf);
		void add(const Frame &a);

		//Output
		friend ostream& operator<<(ostream &os, const chemicalsystem &sys);	

		//Averages
		
		void generate_data();
			
		int max_atoms() const;
		int min_atoms() const;
		double avg_atoms() const;

		void equalize_atom_count();

	private:
		//Variables
		vector <Frame> myframes;

		vector <histogram> hist;

		//Data
		int minat; //min atoms
		int maxat; //max atoms
		double avgat; //average atoms
		vector <string> namelist;
		
		//Reading
		atom readatomPDB(const string &fname);
};
#endif
