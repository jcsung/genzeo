#include "atom.h"
#include "chemicalsystem.h"
#include "frame.h"
#include <string>
#include <iostream>
#include "util.h"
#include <sstream>
#include <fstream>
#include "pdbreader.h"
#include "xtalpdbdata.h"

using namespace util;
using namespace std;

PDBReader::PDBReader()
{
	
}

chemicalsystem PDBReader::read(const string &fin){
	
	int size;
	string junk;
	int x, y=0;
	string str, str2;
	bool flag=false;

	chemicalsystem cs;
	Frame f;
	atom at;
	vector <string> split;
	xtalpdbdata data;

	//cout<<"Read begin"<<endl;
	ifstream in(fin.c_str());
	while (getline(in,str)){
		//cout<<str<<endl;
		flag=false;
		split=splitter(str);
		//Crystal data
		if (split[0]=="CRYST1"){
			//cout<<"Read in crystal data.  ";
			//Otherwise:
			//1. Get the crystal data and set it
			data.a=str2double(split[1]);
			data.b=str2double(split[2]);
			data.c=str2double(split[3]);
			data.alpha=str2double(split[4]);
			data.beta=str2double(split[5]);
			data.gamma=str2double(split[6]);
			data.z=str2int(split[split.size()-1]);
			data.sGroup="";
			for (x=7; x<split.size()-1; x++) data.sGroup+=split[x];
			f.updateXtal(data);
			f.setImageConvention(1);
			//cout<<"Crystal data set for frame"<<endl;
		}
		//ATOM or HETATM field
		else if (split[0]=="ATOM"||split[0]=="HETATM"){
			//cout<<"ATOM or HETATM field"<<endl;
			//1. Parse atom
			while (str.size()<80) str+=" ";
		        at.record=trim(str.substr(0,6));
		        at.serial=str2int(trim(str.substr(6,5)));
		        at.name=trim(str.substr(12,4));
		        at.altLoc=str[16];
		        at.resName=trim(str.substr(17,3));
		        at.chainID=str[21];
		        at.resSeq=str2int(trim(str.substr(22,4)));
		        at.iCode=str[26];
		        at.x=str2double(trim(str.substr(30,8)));
		        at.y=str2double(trim(str.substr(38,8)));
		        at.z=str2double(trim(str.substr(46,8)));
		        at.occupancy=str2double(trim(str.substr(54,6)));
		        at.tempFactor=str2double(trim(str.substr(60,6)));
		        at.element=trim(str.substr(76,2));
		        at.charge=trim(str.substr(78,2));
			//cout<<"Atom parsed"<<endl;
			//2. Add atom to frame
			f.add(at);	
			//cout<<"Atom added to Frame"<<endl;
		}
		//END
		else if (split[0]=="END"){
			//2. Add frame
			cs.add(f);
			//cout<<"Frame added to ChemicalSystem"<<endl;
			//3. Reset frame to be empty
			f.clear();
			//cout<<"Frame cleared"<<endl;
			//4. Set flag to true to show the frame has been added 
			flag=true;
		}
		//Anything else
		else{
			//cout<<"Not CRYST1 or ATOM or HETATM or END field.  Ignored."<<endl;
			continue;
		}
	}
	//cout<<"Loop complete"<<endl;
	//Add the very last frame if one wasn't added
	if (!flag)
		cs.add(f);
	//cout<<"Added the very last frame"<<endl;
	in.close();
	//cout<<"Done"<<endl;
	return cs;
}

