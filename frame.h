#ifndef FRAME_H
#define FRAME_H

#include <fstream>
#include <vector>
#include <string>
#include "atom.h"
#include "xtalpdbdata.h"
#include "rdf.h"
#include "atomtype.h"
#include "forcefield.h"

using namespace std;

class Frame{
	public:
		//Constructor
		Frame();

		//Accessors
		int size() const;				//Size of system
		atom get(int i) const;				//Gets atom at index i (starts at i=1)
		xtalpdbdata getBox() const;			//Gets crystallographic data
		int getImageConvention() const;			//Returns imcon
		vector <double> cellVectors() const;
		string print_cellVectors() const;			//cell vectors
		double getVolume() const;

		//Modifiers
		bool set(int index,const atom &modAtom);	//Sets atom at index i
		void updateXtal(const xtalpdbdata &newCellVec);	//Changes xtalographic data 
		bool updateXtal(const vector <double> &newCellVec); //changes xtalographic data, given the cell
		void setImageConvention(int nimcon);		//Sets new periodicity
		void add(const atom &a);
		void clear();					//Deletes all atoms

		//System properties
		double distance(const atom &a1,const atom &a2);			//Distance between two atoms
		double angle(const atom &a1,const atom &a2,const atom &a3);		//Angle between three atoms
		double dihedral(const atom &a1,const atom &a2,const atom &a3,const atom &a4);	//Dihedral between four atoms
		
		rdf compute_rdf(const string &t1, const string &t2, double dr, double rmin);
		rdf compute_rdf(const string &t1, const string &t2, double dr, double rmin, double rmax);

		double mass() const; //Calculate the mass of the system
		atom com() const; //Calculate the center of mass

		//MCCCS stuff
		vector <atomtype> get_types() const;  //Get types
		void update_get_types(const string &fffile);  //Generate the atom types
		vector <int> get_types_count() const; //Get types count


		//Output
		friend ostream& operator<<(ostream &os, const Frame &f);

	private:
		//Main stuff
		vector <atom> sys;
		xtalpdbdata boxdata;
		vector <double> cellvec;
		int imcon;

		ForceField ff;	
		vector <int> atomtypescount;

		//Functions
		void generateVectors();
		vector <double> deviations(const atom &a1, const atom &a2);
		void standardize_atom_lengths();
	};
#endif
