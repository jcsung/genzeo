#ifndef XYZWRITER_H
#define XYZWRITER_H
#include "chemicalsystem.h"
#include "frame.h"
#include "systemwriter.h"
#include "atom.h"
class XYZWriter : public SystemWriter
{
	public:
		XYZWriter();
		void write(const string &fout, chemicalsystem &sys, bool ONEFILE);
		void write(ofstream &fout, Frame &f, bool ONEFILE);
};
#endif
