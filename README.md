This code was my library for reading and handling molecular simulation structure
files.  

It had general reading and writing superclasses that were extended to read and 
write PDB, XYZ, and in-house simulation structure file formats.  Handling the 
structure files in an object-oriented fashion allowed for the various objects 
to handle properties such as distances, angles, and dihedral angles very easy 
regardless of boundary conditions (of which simple cubic and orthorhombic were 
implemented).

It should be noted that rdf.h does not work at all, and was kept primarily as 
reference.  

As of the last commit, main() was being used to (accurately) compute radial 
distribution functions.
