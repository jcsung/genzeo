
#ifndef ATOM_H
#define ATOM_H

#include <string>
#include <iostream>

using namespace std;

struct atom{
	string record;
	int serial;
	string name;
	char altLoc;
	string resName;
	char chainID;
	int resSeq;
	char iCode;
	double x, y, z;
	double occupancy;
	double tempFactor;
	string element;
	string charge;

	double mass;
	double q;
	int box;
	
	friend ostream &operator<<(ostream &os,const atom &at);
//	friend istream &operator>>(istream &is,const atom &at); //Not sure how to implement this well.
	friend bool operator==(atom &a1,atom &a2);
};
#endif
