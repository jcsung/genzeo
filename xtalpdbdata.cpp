#include "xtalpdbdata.h"
#include <string>
#include <iostream>
#include <iomanip>

using namespace std;

ostream &operator<<(ostream &os, const xtalpdbdata &at){
	string temp=at.sGroup;
	os<<setiosflags(ios_base::right|ios_base::fixed);
	os<<"CRYST1";
	os<<setw(9)<<setprecision(3)<<at.a;
	os<<setw(9)<<setprecision(3)<<at.b;
	os<<setw(9)<<setprecision(3)<<at.c;
	os<<setw(7)<<setprecision(2)<<at.alpha;
	os<<setw(7)<<setprecision(2)<<at.beta;
	os<<setw(7)<<setprecision(2)<<at.gamma;
	while (temp.length()<11) temp=" "+temp;
	os<<temp;
	os<<setw(4)<<at.z;

	return os;
}

bool operator==(xtalpdbdata &at1, xtalpdbdata &at2){
	return at1.a==at2.b&&at1.b==at2.b&&at1.c==at2.c&&at1.alpha==at2.alpha&&at1.beta==at2.beta&&at1.gamma==at2.gamma; 
}
