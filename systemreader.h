#ifndef SYSTEMREADER_H
#define SYSTEMREADER_H
#include <vector>
#include <string>
#include <fstream>
#include "chemicalsystem.h"
class SystemReader{
	public:
		SystemReader();
		virtual chemicalsystem read(const string &fin) = 0;
};
#endif
