#include "chemicalsystem.h"
#include <string>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>
#include "util.h"
#include <fstream>
#include <sstream>

using namespace util;
using namespace std;

chemicalsystem::chemicalsystem(){
	myframes.resize(0);
	minat=0;
	maxat=0;
	avgat=0;
	namelist.resize(0);
}

int chemicalsystem::size() const{
	return myframes.size();
}

Frame chemicalsystem::get(int i) const{
	return myframes[i-1];
}

bool chemicalsystem::set(int index, const Frame &modAtom){
	if (index<=0||index>myframes.size()) return false;
	myframes[index-1]=modAtom;
	return true;
}

void chemicalsystem::add(const Frame &a){
	myframes.resize(myframes.size()+1);
	myframes[myframes.size()-1]=a;
}

atom chemicalsystem::readatomPDB(const string &str2){
	string str=str2;
	while (str.size()<80) str+=" ";
	atom at;
	at.record=trim(str.substr(0,6));
	at.serial=str2int(trim(str.substr(6,5)));
	at.name=trim(str.substr(12,4));
	at.altLoc=str[16];
	at.resName=trim(str.substr(17,3));
	at.chainID=str[21];
	at.resSeq=str2int(trim(str.substr(22,4)));
	at.iCode=str[26];
	at.x=str2double(trim(str.substr(30,8)));
	at.y=str2double(trim(str.substr(38,8)));
	at.z=str2double(trim(str.substr(46,8)));
	at.occupancy=str2double(trim(str.substr(54,6)));
	at.tempFactor=str2double(trim(str.substr(60,6)));
	at.element=trim(str.substr(76,2));
	at.charge=trim(str.substr(78,2));
	return at;
}

ostream &operator<<(ostream &os, const chemicalsystem &sys){
	int x, y;
	os<<sys.get(1).getBox()<<endl;
	for (x=0; x<sys.size(); x++){
		for (y=0; y<sys.get(x+1).size(); y++){
			os<<sys.get(x+1).get(y+1)<<endl;
		}
		os<<"END"<<endl;
	}	
	return os;
}

void chemicalsystem::generate_data(){
	int x, y, z;
	Frame cur;
	string curstr;
	bool flag;
	int sum=0;
	for (x=0; x<size(); x++){
		cur=myframes[x];
		sum+=cur.size();
		if (x==0){
			minat=cur.size();
			maxat=cur.size();
		}
		if (minat>cur.size()){
			minat=cur.size();
		}
		if (maxat<cur.size()){
			maxat=cur.size();
		}
		for (y=0; y<cur.size(); y++){
			curstr=cur.get(y+1).name;
			flag=true;
			for (z=0; z<namelist.size(); z++){
				if (curstr==namelist[z]){
					flag=false;
					break;
				}
			}
			if (flag){
				namelist.resize(namelist.size()+1);
				namelist[namelist.size()-1]=curstr;
			}
		}
	}
	avgat=((double)sum)/size();
	//Ok now minat, maxat, avgat, and namelist are set.  
}

int chemicalsystem::max_atoms() const{
	return maxat;
}

int chemicalsystem::min_atoms() const{
	return minat;
}

double chemicalsystem::avg_atoms() const{
	return avgat;
}

void chemicalsystem::equalize_atom_count(){
//	Precondition: generate_data() was run first
	int x, y;
	atom temp;
	temp.record="ATOM";
	temp.name="X";
	temp.altLoc=' ';
	temp.resName="FAK";
	temp.chainID=' ';
	temp.resSeq=0;
	temp.iCode=' ';
	temp.x=200;
	temp.y=200;
	temp.z=200;
	temp.occupancy=1;
	temp.tempFactor=1;
	temp.element="";
	temp.charge="";
	for (x=0; x<size(); x++){
		temp.serial=myframes[x].size()+1;
		while (myframes[x].size()<maxat){
			myframes[x].add(temp);
			temp.serial++;
		}
	}	
}
