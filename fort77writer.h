#ifndef FORT77READER_H
#define FORT77READER_H

#include "systemwriter.h"
#include "chemicalsystem.h"
class fort77Writer : public SystemWriter
{
	public:
		fort77Writer();
		void write(const string &fout, chemicalsystem &sys, bool ONEFILE);
                void write(ofstream &fout, Frame &f, bool ONEFILE);
};
#endif
