#include "forcefield.h"
#include <string>
#include <iostream>
#include <sstream>
#include "util.h"

using namespace std;
using namespace util;

ForceField::ForceField(){
	atomtypes.resize(1);
}

vector <atomtype> ForceField::getTypes() const{
	return atomtypes;
}

void ForceField::read(const string &fin){
	//Currently only reads atom types
	string str;
	string word;
	bool flag=false;
//	stringstream ss;
	ifstream infile(fin.c_str());
	int x=0;
	atomtypes.resize(1);
	for (;;){
		getline(infile,str);
		if (str=="END ATOMS"){
			//cout<<"COMPLETE"<<endl;
			break;
		}
		if (flag){
			//cout<<"PARSING LINE"<<endl;
			//Parse line
			//infile>>atomtypes[x++];
			stringstream ss(str);
			//ss>>atomtypes[x++];
			atomtypes[x].count=0;
			ss>>word;//cout<<" currently is "<<word<<" "<<ss.eof()<<endl;
			atomtypes[x].index=str2int(word);
			ss>>word;//cout<<" currently is "<<word<<" "<<ss.eof()<<endl;
			atomtypes[x].type=str2int(word);
			ss>>word;//cout<<" currently is "<<word<<" "<<ss.eof()<<endl;
			atomtypes[x].epsilon=str2double(word);
			ss>>word;//cout<<" currently is "<<word<<" "<<ss.eof()<<endl;
			atomtypes[x].sigma=str2double(word);
			ss>>word;//cout<<" currently is "<<word<<" "<<ss.eof()<<endl;
			atomtypes[x].charge=str2double(word);
			ss>>word;//cout<<" currently is "<<word<<" "<<ss.eof()<<endl;
			atomtypes[x].mass=str2double(word);
			ss>>word;//cout<<" currently is "<<word<<" "<<ss.eof()<<endl;
			atomtypes[x].chemid=word;
			if (!ss.eof()){
				ss>>word;//cout<<" currently is "<<word<<" "<<ss.eof()<<endl;
			}
			else{
				word="";
			}
			atomtypes[x++].chname=word;
			if (x==atomtypes.size()){
				atomtypes.resize(2*x);
			}
		}
		if (str=="ATOMS"){
			//cout<<"ATOMS FOUND"<<endl;
			flag=true;
			getline(infile,str); //Read the i type(i) epsi(i) [...] line 
		}
	}
	atomtypes.resize(x);
	infile.close();
}

ostream &operator<<(ostream &os, const ForceField &f){
	os<<"ATOMS"<<endl;
	os<<"!i  type(i)  epsi(i)   sigi(i)   q(i)    mass(i) chemid(i) chname(i) [Angstrom, Kelvin, a.u.]; type 1: Lennard-Jones"<<endl;
	vector <atomtype> atomtypes=f.getTypes();
	for (int x=0; x<atomtypes.size(); x++){
		os<<atomtypes[x]<<endl;
	}
	os<<"END ATOMS";
	return os;
}

