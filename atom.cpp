#include "atom.h"
#include <string>
#include <iostream>
#include <iomanip>

using namespace std;

ostream &operator<<(ostream &os, const atom &at){
	int i;
	string record1=at.record;

	os<<setiosflags(ios_base::right|ios_base::fixed);
	while (record1.length()<6) record1+=" ";
	os<<setw(6)<<record1;
	os<<setw(5)<<at.serial;
	os<<' ';
	os<<setw(4)<<at.name;
	os<<setw(1)<<at.altLoc;
	os<<setw(3)<<at.resName;
	os<<' ';
	os<<setw(1)<<at.chainID;
	os<<setw(4)<<at.resSeq;
	os<<setw(1)<<at.iCode;
	os<<"   ";
	os<<setw(8)<<setprecision(3)<<at.x;
	os<<setw(8)<<setprecision(3)<<at.y;
	os<<setw(8)<<setprecision(3)<<at.z;
	os<<setw(6)<<setprecision(2)<<at.occupancy;
	os<<setw(6)<<setprecision(2)<<at.tempFactor;
	//os<<"  ";
	os<<"          ";
	os<<setw(2)<<at.element;
	os<<setw(2)<<at.charge;
	return os;
}

bool operator==(atom &a1, atom &a2){
	return a1.name==a2.name;
}
