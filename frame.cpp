#include "frame.h"
#include <string>
#include <iostream>
#include <iomanip>
#include <cmath>
#include "util.h"
#include <sstream>

using namespace util;
using namespace std;

Frame::Frame(){
	sys.resize(0);
	cellvec.resize(9);
	for (int x=0; x<9; x++) cellvec[x]=0.0;
	imcon=0;
	boxdata.a=1.0;
	boxdata.b=1.0;
	boxdata.c=1.0;
	boxdata.alpha=90.0;
	boxdata.beta=90.0;
	boxdata.gamma=90.0;
	boxdata.sGroup="P 1";
	boxdata.z=1;
	generateVectors();
}

int Frame::size() const{
	return sys.size();
}

atom Frame::get(int i) const{
	return sys[i-1];
}

xtalpdbdata Frame::getBox() const{
	return boxdata;
}

int Frame::getImageConvention() const{
	//Returns the image convention.
	//Image convention convention taken from DL_POLY 4.02
	//imcon = 0 ==> no periodic boundaries
	//imcon = 1 ==> cubic boundary conditions
	//imcon = 2 ==> orthorhombic boundary conditions
	//imcon = 3 ==> parallelpiped boundary conditions
	//imcon = 6 ==> x-y parallelogram boundary conditions with no periodicity in the z direction
	//NOTE: ONLY IMCON = 0, 1, AND 3 ARE IMPLEMENTED AT THIS TIME.
	return imcon;
}

bool Frame::set(int index, const atom &modAtom){
	if (index<=0||index>sys.size()) return false;
	sys[index-1]=modAtom;
	return true;
}

void Frame::add(const atom &a){
	sys.resize(sys.size()+1);
	sys[sys.size()-1]=a;
}

void Frame::updateXtal(const xtalpdbdata &newCellVec){
	boxdata.a=newCellVec.a;
	boxdata.b=newCellVec.b;
	boxdata.c=newCellVec.c;
	boxdata.alpha=newCellVec.alpha;
	boxdata.beta=newCellVec.beta;
	boxdata.gamma=newCellVec.gamma;
	boxdata.sGroup=newCellVec.sGroup;
	boxdata.z=newCellVec.z;

	generateVectors();

}

bool Frame::updateXtal(const vector <double> &newCellVec){
	bool flag=false;
	int x;
	for (x=0; x<newCellVec.size()&&x<9; x++){
		cellvec[x]=newCellVec[x];
	}
	if (x==9) flag=true;

	//Set boxdata
	double Ax=cellvec[0];
	double Ay=cellvec[1];
	double Az=cellvec[2];
	double Bx=cellvec[3];
	double By=cellvec[4];
	double Bz=cellvec[5];
	double Cx=cellvec[6];
	double Cy=cellvec[7];
	double Cz=cellvec[8];
	double ab = Ax*Bx+Ay*By+Az*Bz;
	double ac = Ax*Cx+Ay*Cy+Az*Cz;
	double bc = Bx*Cx+By*Cy+Bz*Cz;

	boxdata.a=sqrt(Ax*Ax+Ay*Ay+Az*Az);
	boxdata.b=sqrt(Bx*Bx+By*By+Bz*Bz);
	boxdata.c=sqrt(Cx*Cx+Cy*Cy+Cz*Cz);
	boxdata.alpha=rad2deg(acos(bc/(boxdata.b*boxdata.c)));
	boxdata.beta=rad2deg(acos(ac/(boxdata.a*boxdata.c)));
	boxdata.gamma=rad2deg(acos(ab/(boxdata.a*boxdata.b)));

	return flag;
}

void Frame::generateVectors(){
	//Precondition: a, b, c, alpha, beta, and gamma are set
	//  a is assumed to point in the x-direction
	//  alpha is the angle between b and c
	//  beta is the angle between a and c
	//  gamma is the angle between a and b
	//  Algorithm taken from PBCTools.tcl for VMD code
	//  Using the VMD version > 1.8.2 version
	//Postcondition: cellvec is set

	double cosBC=cos(deg2rad(boxdata.alpha));
	double sinBC=sin(deg2rad(boxdata.alpha));
	double cosAC=cos(deg2rad(boxdata.beta));
	double cosAB=cos(deg2rad(boxdata.gamma));
	double sinAB=sin(deg2rad(boxdata.gamma));

	double Ax=boxdata.a;
	double Bx=boxdata.b*cosAB;
	double By=boxdata.b*sinAB;

	double Cx,Cy,Cz;
	//if sinAB = 0, C cannot be uniquely determined since it's defined in terms of the angle between A and B.
	if (sinAB>0){
		/*
		//Idiotic.  This is for VMD versions before 1.4.2
		Cx=cosAC*boxdata.c;
		Cy=(cosBC-cosAC*cosAB)/sinAB*boxdata.c;
		Cz=sqrt(1.0-Cx*Cx-Cy*Cy)*boxdata.c;
		*/
		Cx=cosAC;
		Cy=(cosBC-cosAC*cosAB)/sinAB;
		Cz=sqrt(1.0-Cx*Cx-Cy*Cy);
	}
	else{
		cerr<<"ERROR: Unable to uniquely determine c"<<endl;
		Cx=0.0;
		Cy=0.0;
		Cz=0.0;
	}
/*
	cout<<"cosBC="<<cosBC<<endl;
	cout<<"sinBC="<<sinBC<<endl;
	cout<<"cosAC="<<cosAC<<endl;
	cout<<"cosAB="<<cosAB<<endl;
	cout<<"sinAB="<<sinAB<<endl;
	cout<<"cosAC*cosAB="<<cosAC*cosAB<<endl;
	cout<<"cosBC-cosAC*cosAB="<<cosBC-cosAC*cosAB<<endl;
	cout<<"Cy="<<(cosBC-cosAC*cosAB)/sinAB<<endl;
*/
	cellvec[0]=Ax;
	cellvec[1]=0;
	cellvec[2]=0;
	cellvec[3]=Bx;
	cellvec[4]=By;
	cellvec[5]=0;
	cellvec[6]=Cx*boxdata.c;
	cellvec[7]=Cy*boxdata.c;
	cellvec[8]=Cz*boxdata.c;

}

string Frame::print_cellVectors() const{
	string str="";
	for (int i=0; i<9; i++){
		str+=double2str(cellvec[i])+" ";
		if (i%3==2) str+="\n";
	}
	return str;
}

vector <double> Frame::cellVectors() const{
	return cellvec; 
}

void Frame::setImageConvention(int nimcon){
	imcon=nimcon;
}

vector <double> Frame::deviations(const atom &a1, const atom &a2){
	double dx=a1.x-a2.x;
	double dy=a1.y-a2.y;
	double dz=a1.z-a2.z;

	//cout<<"atom 1: "<<a1.x<<" "<<a1.y<<" "<<a1.z<<endl;
	//cout<<"atom 2: "<<a2.x<<" "<<a2.y<<" "<<a2.z<<endl;
	//cout<<"dx="<<dx<<" dy="<<dy<<" dz="<<dz<<endl;

	if (imcon==0){
		//No periodic boundaries
		//Already done
		//cout<<"dx="<<dx<<" dy="<<dy<<" dz="<<dz<<endl;
	}
	else if(imcon==1){
		//Cubic boundaries
		dx-=cellvec[0]*floor(dx/cellvec[0]+0.5);
		dy-=cellvec[4]*floor(dy/cellvec[4]+0.5);
		dz-=cellvec[8]*floor(dz/cellvec[8]+0.5);
		//cout<<"In imcon==1 "<<"dx="<<dx<<" dy="<<dy<<" dz="<<dz<<endl;
	}
	else{ //if(imcon==3)
		//Parallelpipid

		//Invert matrix
		vector <double> rcell(9);
		rcell[0]=cellvec[4]*cellvec[8]-cellvec[5]*cellvec[7];
		rcell[1]=cellvec[2]*cellvec[7]-cellvec[1]*cellvec[8];
		rcell[2]=cellvec[1]*cellvec[5]-cellvec[2]*cellvec[4];
		rcell[3]=cellvec[5]*cellvec[6]-cellvec[3]*cellvec[8];
		rcell[4]=cellvec[0]*cellvec[8]-cellvec[2]*cellvec[6];
		rcell[5]=cellvec[2]*cellvec[3]-cellvec[0]*cellvec[5];
		rcell[6]=cellvec[3]*cellvec[7]-cellvec[4]*cellvec[6];
		rcell[7]=cellvec[1]*cellvec[6]-cellvec[0]*cellvec[7];
		rcell[8]=cellvec[0]*cellvec[4]-cellvec[1]*cellvec[3];
		double determinant=cellvec[0]*rcell[0]+cellvec[3]*rcell[1]+cellvec[6]*rcell[2];
		double detinv=0;
		if (abs(determinant)>0) detinv=1.0/determinant;

		for (int x=0; x<9; x++) rcell[x]*=detinv;

		//cout<<"dx="<<dx<<" dy="<<dy<<" dz="<<dz<<endl;

		//do it
		double ssx=rcell[0]*dx+rcell[3]*dy+rcell[6]*dz;
		double ssy=rcell[1]*dx+rcell[4]*dy+rcell[7]*dz;
		double ssz=rcell[2]*dx+rcell[5]*dy+rcell[8]*dz;

		//cout<<"ssx="<<ssx<<" ssy="<<ssy<<" ssz="<<ssz<<endl;

		//cout<<"ssx+0.5="<<ssx+0.5<<" ssy+0.5="<<ssy+0.5<<" ssz+0.5="<<ssz+0.5<<endl;

		//cout<<"floor(ssx+0.5)="<<floor(ssx+0.5)<<" floor(ssy+0.5)="<<floor(ssy+0.5)<<" floor(ssz+0.5)="<<floor(ssz+0.5)<<endl;
		//cout<<"ssx-floor(ssx+0.5)="<<ssx-floor(ssx+0.5)<<" ssy-floor(ssy+0.5)="<<ssy-floor(ssy+0.5)<<" ssz-floor(ssz+0.5)="<<ssz-floor(ssz+0.5)<<endl;

		double xss=ssx-floor(ssx+0.5);
		double yss=ssy-floor(ssy+0.5);
		double zss=ssz-floor(ssz+0.5);

		//cout<<"xss="<<xss<<" yss="<<yss<<" zss="<<zss<<endl;
	
		dx=cellvec[0]*xss+cellvec[3]*yss+cellvec[6]*zss;
		dy=cellvec[1]*xss+cellvec[4]*yss+cellvec[7]*zss;
		dz=cellvec[2]*xss+cellvec[5]*yss+cellvec[8]*zss;

		//cout<<"dx="<<dx<<" dy="<<dy<<" dz="<<dz<<endl;

	}

	vector <double> ret(3);
	ret[0]=dx;
	ret[1]=dy;
	ret[2]=dz;
	return ret;
}

double Frame::distance(const atom &a1, const atom &a2){
//	cout<<"calculating distance"<<endl;
	double dist=0;
	
	vector <double> dr=deviations(a1,a2);
	double dx=dr[0];
	double dy=dr[1];
	double dz=dr[2];

	dist=sqrt(dx*dx+dy*dy+dz*dz);

	//if(dist>=0.5*cellvec[0]) return cellvec[0]-dist;
	return dist;
}

double Frame::angle(const atom &a1, const atom &a2, const atom &a3){
	//p1 is the vector from a2 to a1
	//p3 is the vector from a2 to a3
	//

	vector <double> dp1=deviations(a1,a2);
	vector <double> dp3=deviations(a3,a2);

	atom fake1, fake3;

	fake1.x=a2.x+dp1[0];
	fake1.y=a2.y+dp1[1];
	fake1.z=a2.z+dp1[2];

	fake3.x=a2.x+dp3[0];
	fake3.y=a2.y+dp3[1];
	fake3.z=a2.z+dp3[2];

	//Now use the dot product
	//a.b = a*b*cos(theta)
	
	double d1=distance(fake1,a2);
	double d3=distance(fake3,a2);

	double sum=fake1.x*fake3.x+fake1.y*fake3.y+fake1.z*fake3.z;

	return acos(sum/(d1*d3));
}

double Frame::dihedral(const atom &a1, const atom &a2, const atom &a3, const atom &a4){
	//p1 is the vector from a2 to a1
	//p4 is the vector from a3 to a4
	//

	vector <double> dp1=deviations(a1,a2);
	vector <double> dp4=deviations(a4,a3);

	atom fake1, fake4;

	fake1.x=a2.x+dp1[0];
	fake1.y=a2.y+dp1[1];
	fake1.z=a2.z+dp1[2];

	fake4.x=a2.x+dp4[0];
	fake4.y=a2.y+dp4[1];
	fake4.z=a2.z+dp4[2];

	//Now use the dot product
	//a.b = a*b*cos(theta)
	
	double d1=distance(fake1,a2);
	double d4=distance(fake4,a3);

	double sum=fake1.x*fake4.x+fake1.y*fake4.y+fake1.z*fake4.z;

	return acos(sum/(d1*d4));
}

double Frame::getVolume() const{
	double vol;
	if (imcon==0){
		//Aperiodic system
		//No reasonable volume.  Return negative value
		vol=-1;
	}
	else if (imcon==1){
		//Cubic system
		//Return a*b*c
		vol=cellvec[0]*cellvec[4]*cellvec[8];
	}
	else /*if (imcon==3)*/{
		//Parallelpipid system
		//Use a.bxc
		vol =cellvec[0]*(cellvec[4]*cellvec[8]-cellvec[7]*cellvec[5]);
		vol+=cellvec[3]*(cellvec[1]*cellvec[8]-cellvec[7]*cellvec[2]);
		vol+=cellvec[6]*(cellvec[1]*cellvec[5]-cellvec[4]*cellvec[2]);
	}
	return vol;
}

void Frame::update_get_types(const string &fffile){
	int x, y, z=0;
	ff.read(fffile);
	vector <atomtype> atomtypes=ff.getTypes();
	z=atomtypes.size();
	//2. Get counts
	atom at;
	atomtypescount.resize(z);
//	cout<<"Get atom type counts"<<endl;
	for (x=0; x<atomtypescount.size(); x++){
		atomtypescount[x]=0;
		for (y=0; y<sys.size(); y++){
			at=sys[y];
			if (at.name==atomtypes[x].chemid){
				atomtypescount[x]++;
				sys[y].q=atomtypes[x].charge;
			}
		}
	}

/*
	cout<<"[";
	for (x=0; x<atomtypes.size(); x++){
		cout<<atomtypes[x];
		if (x<atomtypes.size()-1) cout<<",";
	}	
	cout<<"]"<<endl;
*/
}

vector <int> Frame::get_types_count() const{
	return atomtypescount;
}

rdf Frame::compute_rdf(const string &t1, const string &t2, double dr, double rmin){
	double max=boxdata.a;
	if (boxdata.b>max) max=boxdata.b;
	if (boxdata.c>max) max=boxdata.c;
	max/=2.0;
	max=ceil(max);
	return compute_rdf(t1,t2,dr,rmin,max);
}

rdf Frame::compute_rdf(const string &t1, const string &t2, double dr, double rmin, double rmax){

//	1. Computation of bins
	int x, y;

//	  A. Get indices of type 1 and 2	
	
//	  B. Bin them
	int numpair=0;
	double dist;
	int whichbin;
	int numbin=(int)((rmax-rmin)/dr)+1;
	cout<<"Number of bins: "<<numbin<<endl;
	vector <int> mybins(numbin);
	for (x=0; x<numbin; x++){
		mybins[x]=0;
	}
	for (x=0; x<sys.size(); x++){
		for (y=0; y<sys.size(); y++){
			//Make sure not looking at self
			if (x==y) continue;
			//Otherwise...
		}
	}
	//cout<<"done"<<endl;
//	return mybins;	
/*
	vector <int> nint(numbin);
	int sum=0;
	for (x=0; x<numbin; x++){
//		cout<<x*dr<<" "<<mybins[x]<<endl;
		sum+=mybins[x];
		nint[x]=sum;
	}


	double prefactor=getVolume()/numpair;

	vector <double> dVol(numbin);
	double r, rlo, rhi;
	for (x=0; x<numbin; x++){
		r=rmin+dr*(x+0.5);
		rlo=r-dr/2.0;
		rhi=r+dr/2.0;
		dVol[x]=4.0/3.0*pi()*(pow(rhi,3)-pow(rlo,3));
	}

	vector <double> radiald(numbin);

	for (x=0; x<numbin; x++){
		radiald[x]=getVolume()/numpair*mybins[x]/dVol[x];
//		cout<<x*dr<<" "<<radiald[x]<<endl;
	}

	rdf rr(numbin,mybins,nint,radiald,rmin,rmax,dr);
*/
	vector <double> radiald(0);
	rdf rr(0,mybins,mybins,radiald,0,0,0);
	return rr;	
}

ostream &operator<<(ostream &os, const Frame &f){
	os<<f.boxdata<<endl;
	for (int x=0; x<f.size(); x++){
		os<<f.get(x+1)<<endl;
	}
	return os;
}

void Frame::clear(){
	sys.resize(0);
}

double Frame::mass() const{
	double m=0;
	for (int i=0; i<sys.size(); i++){
		m+=sys[i].mass;
	}
	//cout<<"mass="<<m<<endl;
	return m;
}

atom Frame::com() const{
	atom at;
	at.record="ATOM";
	at.serial=0;
	at.name="X";
	at.altLoc=' ';
	at.resName="COM";
	at.chainID=' ';
	at.resSeq=0;
	at.iCode=' ';
	at.x=0;
	at.y=0;
	at.z=0;
	at.occupancy=0;
	at.tempFactor=0;
	at.element="X";
	at.charge="";
	at.mass=mass();
	for (int i=0; i<sys.size(); i++){
		at.x+=sys[i].mass*sys[i].x;
		at.y+=sys[i].mass*sys[i].y;
		at.z+=sys[i].mass*sys[i].z;
	}
	at.x/=at.mass;
	at.y/=at.mass;
	at.z/=at.mass;
	return at;

}	

void Frame::standardize_atom_lengths(){
//	cout<<"***Standardizing Atom Lengths"<<endl;
	int max=0, x, y;
	for (x=0; x<sys.size(); x++){
//		cout<<"x="<<x<<" sys[x].name="<<sys[x].name<<" sys[x].name.size()="<<sys[x].name.size()<<" max=";
//		cout<<max<<" ((int)((sys[x].name).size())>max?="<<((int)((sys[x].name).size())>max);
//		cout<<" 1>-1?="<<(1>-1)<<endl;
		if (max<((sys[x].name).size())){
//What the hell?  Don't remove the (int) cast or this breaks and max stays as -1.
			cout<<"in here"<<endl;
			max=sys[x].name.size();
		}
	}
//	cout<<"***Max="<<max<<endl;
	for (x=0; x<sys.size(); x++){
		while ((int)((sys[x].name).size())<max){	
			sys[x].name+=" ";
		}
	}
}

vector <atomtype> Frame::get_types() const{
	return ff.getTypes();
}

/*
rdf Frame::genrdf(const string &t1, const string &t2, double dr, double rmin, double rmax){
	int x, y, count1=0, count2=0, whichbin;
	//Create blank rdf object
	rdf myrdf(dr,rmin,rmax);
	//Create vectors of indices of atoms 1 and 2.
	vector <int> type1(1);
	vector <int> type2(1);
	for (x=0; x<sys.size(); x++){
		cout<<"sys["<<x<<"]='"<<sys[x].name<<"'"<<endl;
		if (sys[x].name==t1) type1[count1++]=x;
		if (sys[x].name==t2) type2[count2++]=x;
		if (count1==type1.size()) type1.resize(2*count1);
		if (count2==type2.size()) type2.resize(2*count2);
	}
	type1.resize(count1);
	type2.resize(count2);
	//Bin them
	double dist;
	for (x=0; x<type1.size(); x++){
		for (y=0; y<type2.size(); y++){
			if (type1[x]==type2[y]){
				count2--; //because later we'll take count1*count2 as the #pair
				continue;
			}
			cout<<"type1["<<x<<"]="<<type1[x]<<" type1[one]="<<type1[1]<<" type2["<<y<<"]="<<type2[y];
			dist=distance(sys[type1[x]],sys[type2[y]]);
			whichbin=(int)((dist-rmin)/dr);
			cout<<" d="<<dist<<" bin="<<whichbin<<" numbins: "<<myrdf.size()<<endl;
			myrdf.increment(whichbin);
		}
	}
	//Add volume information, number of pairs, to set normalization factor
	myrdf.setVolume(getVolume());
	myrdf.setPairs(count1*count2);
	myrdf.setPrefactor(myrdf.getVolume()/myrdf.getPairs());
	//Calculate the dvol
	double r, rlo, rhi;
	for (x=0; x<myrdf.size(); x++){
		r=rmin+dr*(x+0.5);
		rlo=r-dr/2.0;
		rhi=r+dr/2.0;
		myrdf.set_dVol(x,4.0*pi()/3.0*(pow(rhi,3)-pow(rlo,3)));
	}
	return myrdf;
}
*/
