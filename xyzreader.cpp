#include "atom.h"
#include "chemicalsystem.h"
#include "frame.h"
#include <string>
#include <iostream>
#include "util.h"
#include <sstream>
#include <fstream>
#include "xyzreader.h"

using namespace util;
using namespace std;

XYZReader::XYZReader()
{
	
}

chemicalsystem XYZReader::read(const string &fin){
	
	int size;
	string junk;
	int x, y=0;

	chemicalsystem cs;
	Frame f;
	atom at;

	ifstream in(fin.c_str());
	while (in>>size){
		if (in.eof()) return cs; 
		in.ignore(100,'\n'); //get newline
		getline(in,junk); //get the info line
		for (x=0; x<size; x++){
			in>>at.name>>at.x>>at.y>>at.z;
			getline(in,junk); //Flush the rest of the line
			at.serial=x+1;
			at.record="ATOM";
			at.altLoc=' ';
			at.resName="MOL";
			at.chainID=' ';
			at.resSeq=0;
			at.iCode=' ';
			at.occupancy=0;
			at.tempFactor=0;
			at.element="";
			at.charge="";
			at.mass=0;
			f.add(at);
		}
		cs.add(f);
		f.clear();
	}
	in.close();
	return cs;
}

