#include <iostream>
#include <fstream>
#include "atom.h"
#include "util.h"
#include "frame.h"
#include "chemicalsystem.h"
#include <fstream>
#include <string>
#include <vector>
#include "systemreader.h"
#include "pdbreader.h"
#include <cmath>
using namespace std;
using namespace util;

int main(int argc,char *argv[]){
	if (argc-1<4){
		cout<<"Syntax: "<<argv[0]<<" PDB_FILE_IN ATOM_TYPE_1 ATOM_TYPE_2 DELTA_R"<<endl;
	}
	else{
		int x, y;
		vector <int> bins(1);
		vector <double> rdf(1);
		vector <double> ndens(1);
		vector <double> realndens(1);
		vector <int> indexOfType1(1);
		vector <int> indexOfType2(1);
		double sysvol;
		double distance;
		int whichbin;
		int i, j, k;
		string frnum;

		ofstream outfile;
		string fout;

		//0. Grab the stuff
		string fin=argv[1];
		string atomtype1=argv[2];
		string atomtype2=argv[3];
		double dbin=str2double(argv[4]);
		double binmin=0;
		double binmax;
		int nbin;

		//1. Read the frame
		SystemReader* r;
		r=new PDBReader();
		cout<<"Reading "<<fin<<endl;
		chemicalsystem cs=r->read(fin.c_str());
		cout<<"Read complete"<<endl;
		delete r;
		Frame f=cs.get(1);
		atom at;
		//cout<<cs<<endl;
		//Get binmax
		vector <double> siz;
		//cout<<fin<<" "<<binmin<<" "<<binmax<<" "<<dbin<<" "<<nbin<<endl;

		for (i=0; i<cs.size(); i++){
			f=cs.get(i+1);
			
			siz=f.cellVectors();
			sysvol=f.getVolume();
			binmax=siz[0]*0.5;
			nbin=(int)((binmax-binmin)/dbin);
			frnum=int2str(i+1);
			while (frnum.size()<int2str(cs.size()).size()){
				frnum="0"+frnum;
			}

			cout<<"Frame #"<<i+1<<" of "<<cs.size()<<endl;
			cout<<f.print_cellVectors()<<endl;
			cout<<"rmin="<<binmin<<endl;
			cout<<"rmax="<<binmax<<endl;
			cout<<"  dr="<<dbin<<endl;
			cout<<"bins="<<nbin<<endl;
			cout<<"Image convention: "<<f.getImageConvention()<<endl;
			cout<<"System volume: "<<sysvol<<endl;

			//2. Get distances and bin them
			//  a.) Reset variables
			bins.resize(nbin);
			rdf.resize(nbin);
			ndens.resize(nbin);
			realndens.resize(nbin);
			for (x=0; x<nbin; x++){
				bins[x]=0;
				rdf[x]=0;
				ndens[x]=0;
				realndens[x]=0;
			}
			//  b.) Generate array of atom type 1
			y=0;
			for (x=0; x<f.size(); x++){
				at=f.get(x+1);
				if (at.name==atomtype1){
					indexOfType1[y++]=x+1;
					if (y==indexOfType1.size()) indexOfType1.resize(2*y);
				}
			}
			indexOfType1.resize(y);
			cout<<y<<" atoms of type "<<atomtype1<<endl;
			//  c.) Generate array of atom type 2
			y=0;
			for (x=0; x<f.size(); x++){
				at=f.get(x+1);
				if (at.name==atomtype2){
					indexOfType2[y++]=x+1;
					if (y==indexOfType2.size()) indexOfType2.resize(2*y);
				}
			}
			indexOfType2.resize(y);
			cout<<y<<" atoms of type "<<atomtype2<<endl;
			//  d.) Calculate distances
			for (x=0; x<indexOfType1.size(); x++){
				for (y=0; y<indexOfType2.size(); y++){
					if (indexOfType1[x]==indexOfType2[y]) continue;
					distance=f.distance(f.get(indexOfType1[x]),f.get(indexOfType2[y]));
					//cout<<"**Comparing "<<indexOfType1[x];
					//cout<<" and "<<indexOfType2[y];
					//cout<<". r = "<<distance<<endl;
					whichbin=(int)((distance-binmin)/dbin);
					if (whichbin<nbin){
						bins[whichbin]++;
						//cout<<"Increment bin "<<whichbin<<endl;
					}
					else{	
						//cout<<"Indexes "<<indexOfType1[x];
						//cout<<" and "<<indexOfType2[y];
						//cout<<" too far at r = "<<distance<<endl;
					}
				}
			}
			
			for (x=0; x<bins.size(); x++){
				cout<<(x==0?"bins=[":"")<<bins[x]<<(x<bins.size()-1?",":"]\n");
			}
			cout<<endl;
			//  e.) Compute RDF stuff
			cout<<"Computing RDF"<<endl;
			for (x=0; x<nbin; x++){
				ndens[x]=(double)bins[x];
				if (atomtype1!=atomtype2){
					ndens[x]/=(double)(indexOfType1.size()*indexOfType2.size());
				}
				else{
					ndens[x]/=(double)(indexOfType1.size()*(indexOfType1.size()-1));
				}
				realndens[x]+=(double)bins[x]/((double)indexOfType1.size());
			}
			for (x=0; x<nbin; x++){
				rdf[x]=ndens[x]*sysvol;
				rdf[x]/=(4*3.14159265/3);
				rdf[x]/=((pow(x+1,3)-pow(x,3))*pow(dbin,3));
			}
			//  # integral
			for (x=1; x<nbin; x++){
				realndens[x]+=realndens[x-1];
			}
			cout<<"RDF calculation complete"<<endl;
			cout<<endl;
			for (x=0; x<ndens.size(); x++){
				cout<<(x==0?"ndens=[":"")<<ndens[x]<<(x<ndens.size()-1?",":"]\n");
			}
			cout<<endl;
			for (x=0; x<rdf.size(); x++){
				cout<<(x==0?"rdf=[":"")<<rdf[x]<<(x<rdf.size()-1?",":"]\n");
			}
			//  f.) Write files
			//      Bins
			//      The r values should be in the middle of the bins
			//      r(n)   = binmin + n     * dbin, n = 0, 1, 2, ...
			//      r(n+1) = binmin + (n+1) * dbin
			//      r = (binmin + n*dbin + binmin + (n+1)*dbin)/2
			//      r = binmin + n * dbin + dbin/2 
			outfile.open(("bins"+frnum+".dat").c_str());
			for (x=0; x<nbin; x++){
				outfile<<binmin+x*dbin+dbin/2.0<<" "<<bins[x]<<endl;
			}
			outfile.close();
			outfile.open(("nden"+frnum+".dat").c_str());
			for (x=0; x<nbin; x++){
				outfile<<binmin+x*dbin+dbin/2.0<<" "<<ndens[x]<<endl;
			}
			outfile.close();
			outfile.open(("nint"+frnum+".dat").c_str());
			for (x=0; x<nbin; x++){
				outfile<<binmin+x*dbin+dbin/2.0<<" "<<realndens[x]<<endl;
			}
			outfile.close();
			outfile.open(("rdf"+frnum+".dat").c_str());
			for (x=0; x<nbin; x++){
				outfile<<binmin+x*dbin+dbin/2.0<<" "<<rdf[x]<<endl;
			}
			outfile.close();
		}
	}
	return 0;
}
