#ifndef XTALPDBDATA_H
#define XTALPDBDATA_H

#include <string>
#include <iostream>

using namespace std;

struct xtalpdbdata{
	double a;
	double b;
	double c;
	double alpha;
	double beta;
	double gamma;
	string sGroup;
	int z;	
	
	friend ostream &operator<<(ostream &os,const xtalpdbdata &at);
//	friend istream &operator>>(istream &is,const xtalpdbdata &at); //Not sure how to implement this well.
	friend bool operator==(xtalpdbdata &a1,xtalpdbdata &a2);
};
#endif
