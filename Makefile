EXE = genzeo.exe 
FILES = util.cpp atom.cpp xtalpdbdata.cpp rdf.cpp frame.cpp chemicalsystem.cpp atomtype.cpp forcefield.cpp systemreader.cpp xyzreader.cpp mcccsmoviereader.cpp systemwriter.cpp xyzwriter.cpp fort77writer.cpp pdbreader.cpp
LOC = .

#  ---------------------
main: $(FILES)
	g++  $(FILES) main.cpp -o $(LOC)/$(EXE)

#  ---------------------
converter: $(FILES)
	g++  $(FILES) movie2pdb.cpp -o $(LOC)/$(EXE)

#  ---------------------
xyz2pdb: $(FILES)
	g++  $(FILES) xyz2pdb.cpp -o $(LOC)/$(EXE)

#  ---------------------
mansi: $(FILES)
	g++  $(FILES) mansi.cpp -o $(LOC)/$(EXE)

#  ---------------------
renamer: $(FILES)
	g++  $(FILES) rename.cpp -o $(LOC)/$(EXE)

#  ---------------------
replicate: $(FILES)
	g++  $(FILES) replicate.cpp -o $(LOC)/$(EXE)

#  ---------------------
rotate: $(FILES)
	g++  $(FILES) rotate.cpp -o $(LOC)/$(EXE)

#  ---------------------
getem: $(FILES)
	g++  $(FILES) getem.cpp -o $(LOC)/$(EXE)

#  ---------------------
tocssr: $(FILES)
	g++  $(FILES) tocssr.cpp -o $(LOC)/$(EXE)

#  ---------------------
blah: $(FILES)
	g++  $(FILES) blah.cpp -o $(LOC)/$(EXE)

#  ---------------------
topdb: $(FILES)
	g++  $(FILES) topdb.cpp -o $(LOC)/$(EXE)

#  ---------------------
negy: $(FILES)
	g++  $(FILES) negy.cpp -o $(LOC)/$(EXE)

#  ---------------------
determinereplicates: $(FILES)
	g++  $(FILES) detrep.cpp -o $(LOC)/$(EXE)

#  ---------------------
clean:
	rm -f $(LOC)/$(EXE) *.mod *.o $(LOC)/*.out
