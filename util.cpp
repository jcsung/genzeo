#include "util.h"
#include <cmath>
#include <iostream>
#include <sstream>
#include <algorithm>
#include <functional>
#include <cctype>
#include <locale>
#include <vector>
using namespace std;

namespace util{

	string int2str(int i){
		stringstream ss;
		ss<<i;
		return ss.str();
	}
	
	int str2int(const string &s){
		stringstream ss(s);
		int i;
		if (!(ss>>i)) i=0;
		return i;
	}

	string double2str(double d){
		stringstream ss;
		ss<<d;
		return ss.str();
	}

	double str2double(const string &s){
		stringstream ss(s);
		double d;
		if (!(ss>>d)) d=0.0;
		return d;
	}

	bool dequals(double a, double b){
		return dequals(a,b,0.000001);
	}

	bool dequals(double a, double b, double tol){
		return abs(a-b)<abs(tol);
	}

	double pi(){
		return 4*atan(1);
	}

	double deg2rad(double ang){
		return ang/180.0*pi();
	}

	double rad2deg(double ang){
		return 180.0*ang/pi();
	}

	string trim(const string &str){
		string s=str;
		//From left
		s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
		//From right
		s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
		return s;
	}

	vector <string> splitter(const string &str){
		//cout<<"Splitting begin"<<endl;
		vector <string> ret(1);
		int x=0;
		stringstream ss(str);
		while(ss){
			//cout<<" x="<<x<<endl;
			ss>>ret[x++];
			//cout<<" ret set"<<endl;
			if (x==ret.size()){
				//cout<<" ret resized"<<endl;
				ret.resize(2*x);
			}
		}
		ret.resize(x);
		//cout<<"Splitting complete"<<endl;
		return ret;
	}	

}
