#include "atom.h"
#include "chemicalsystem.h"
#include "frame.h"
#include <string>
#include <iostream>
#include "util.h"
#include <sstream>
#include <fstream>
#include "mcccsmoviereader.h"
#include "atomtype.h"
#include "forcefield.h"

using namespace util;
using namespace std;

MCCCSMovieReader::MCCCSMovieReader()
{
	
}

chemicalsystem MCCCSMovieReader::read(const string &fin){
	chemicalsystem cs;
	ForceField ff;
	ff.read("topmon.inp");
	vector <atomtype> mytypes=ff.getTypes();
	int x, y, z;
	int nframes, nmolec, ntype, nbox, nbeadtype;
	vector <double> rcut;
	vector <double> usedtypes;
	int count, counted_data, bead_of_type;
		
	ifstream infile(fin.c_str());
	//1. Read basic data (nframe, nmolec, etc.)
	infile>>nframes>>nmolec>>ntype>>nbox>>nbeadtype;
	rcut.resize(nbox);
	usedtypes.resize(nbeadtype);
	//2. Read rcuts and # of bead types
	for (x=0; x<nbox; x++){
		infile>>rcut[x];
	}
	for (x=0; x<nbeadtype; x++){
		infile>>usedtypes[x];
	}
	//3. Read connectivity
	//Note: This is currently discarded.  Mostly.
	for (x=0; x<ntype; x++){
		//Read #beads in molec $x
		infile>>bead_of_type;

		//Read bonds
		for (y=0; y<bead_of_type; y++){
			//Read #bonds for bead $y
			infile>>count;
			for (z=0; z<count; z++){
				//Read bond $z
				infile>>counted_data;
			}
		}
		//Read torsions
		for (y=0; y<bead_of_type; y++){
			//Read #torsions for bead $y
			infile>>count;
			for (z=0; z<count; z++){
				//Read torsion $z
				infile>>counted_data;
			}
		}
	}

	int lcv;
	int ncycle;
	vector <int> serial(nbox);
	vector <int> resSeq(nbox);	
	vector <double> cur_cell(9);
	vector <Frame> cur_frame(nbox);
	//4. Read frames
	for (lcv=0; lcv<nframes; lcv++){
		cout<<"Reading Frame #"<<lcv+1<<"/"<<nframes<<endl;
		//Read number of cycles
		infile>>ncycle;
		//Reset the serial/resSeq
		for (x=0; x<nbox; x++){
			serial[x]=1;
			resSeq[x]=1;
		}
		//Read the number of molecules in each box
		for (x=0; x<nbox; x++){
			for(y=0; y<ntype; y++){
				//Read the number of molecules of type $y in box $x
				infile>>counted_data; //Don't need it so it's discarded
			}
			//Read in the dimensions
			for (y=0; y<cur_cell.size(); y++){
/*
 				//This is for non-cubic boxes but there's no way currently
 				//to distinguish from just the movie1a.dat file...
				if (x==0){
					infile>>cur_cell[y];
				}
				else
*/
				//This is for cubic boxes
				if (y%4==0){
					infile>>cur_cell[y];
				}
				else{
					cur_cell[y]=0;
				}
			}
			cur_frame[x].updateXtal(cur_cell);
			cout<<cur_frame[x].print_cellVectors()<<endl;
		}
	}			
	
	infile.close();
	

	return cs;
}

