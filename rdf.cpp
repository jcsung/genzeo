#include "frame.h"
#include <string>
#include <iostream>
#include <cmath>
#include "util.h"

using namespace util;
using namespace std;

rdf::rdf(int nbin, const vector <int> &thebins, const vector <int> &nintgrl, const vector <double> &rdfn, double min, double max, double dif){
	numbin=nbin;
	rmin=min;
	rmax=max;
	dr=dif;

	mybins.resize(numbin);
	nint.resize(numbin);
	radiald.resize(numbin);
	
	for (int x=0; x<numbin; x++){
		mybins[x]=thebins[x];
		nint[x]=nintgrl[x];
		radiald[x]=rdfn[x];
	}

}

vector <double> rdf::get_bin(int index) const{
	vector <double> ret(2);
	ret[0]=rmin+dr*(index-1);
	ret[1]=(index>0&&index<=numbin?mybins[index-1]:-1);
	return ret;	
}

vector <double> rdf::get_number_integral(int index) const{
	vector <double> ret(2);
	ret[0]=rmin+dr*(index-1);
	ret[1]=(index>0&&index<=numbin?nint[index-1]:-1);
	return ret;	
}

vector <double> rdf::get_rdf(int index) const{
	vector <double> ret(2);
	ret[0]=rmin+dr*(index-1);
	ret[1]=(index>0&&index<=numbin?radiald[index-1]:-1);
	return ret;	
}

string rdf::print_bins() const{
	string str="";
	vector <double> cur;
	int x;
	for (x=1; x<=numbin; x++){
		cur=get_bin(x);
		str+=double2str(cur[0])+" "+double2str(cur[1])+"\n";
	}
	return str;
}

string rdf::print_number_integral() const{
	string str="";
	vector <double> cur;
	int x;
	for (x=1; x<=numbin; x++){
		cur=get_number_integral(x);
		str+=double2str(cur[0])+" "+double2str(cur[1])+"\n";
	}
	return str;
}

string rdf::print_rdf() const{
	string str="";
	vector <double> cur;
	int x;
	for (x=1; x<=numbin; x++){
		cur=get_rdf(x);
		str+=double2str(cur[0])+" "+double2str(cur[1])+"\n";
	}
	return str;
}


