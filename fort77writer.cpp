#include "atom.h"
#include "chemicalsystem.h"
#include "frame.h"
#include <string>
#include <iostream>
#include "util.h"
#include "fort77writer.h"

using namespace util;
using namespace std;

fort77Writer::fort77Writer()
{
	
}

void fort77Writer::write(const string &fout, chemicalsystem &sys, bool ONEFILE){
	if (ONEFILE) cerr<<"multi-file XYZ writing not yet implemented"<<endl;
	Frame f;
	ofstream outfile(fout.c_str());
	for (int x=0; x<sys.size(); x++){
		f=sys.get(x+1);
		write(outfile,f,ONEFILE);
	}
	outfile.close();
}

void fort77Writer::write(ofstream &fout, Frame &f, bool ONEFILE){
	stringstream out;
	int x, y;
	bool flag;

//	cout<<"Write subroutine begin"<<endl;

	//1. Update the atom types
	f.update_get_types("topmon.dat");
	vector <atomtype> atomtypes=f.get_types();
	vector <int> atomtypescount=f.get_types_count();

//	cout<<"Vector sizes set"<<endl;
	out<<0<<endl;					//# of cycles
	out<<0.0<<" "<<0.0<<" "<<0.0<<endl;		//max displacement
	//The following loop needs to happen nbox times
	//But since only 1 box...

//	cout<<"Number of cycles and max displacement written"<<endl;	

	//The two molecule types are
	//molecule type 1: framework
	//molecule type 2: n2
	for (x=0; x<2; x++){	
		out<<0.0<<" "<<0.0<<" "<<0.0<<endl;	//max disp
		out<<0.0<<" "<<0.0<<" "<<0.0<<endl;	//max angle
	}
	//End loop that needs to happen
	//rmflcq - note: this should also happen nbox times
	for (x=0; x<2; x++){
		out<<0.0<<" ";
	}
	out<<endl;
	out<<0.0<<endl;					//rmvol - only needs to happen nbox times

//	cout<<"Max displacement, max angle, rmflcq, and rmvol written"<<endl;	

	out<<1000.0<<" "<<1000.0<<" "<<1000.0<<endl;	//box size
	out<<2<<endl;				//nchain - total number of chains = 2
	out<<2<<endl;			//nmolty - number of atom types

	//read(io_restart,*) nures(1:nmtres)
	out<<"38 3"<<endl;

//	cout<<"Box size, nchain, and nmolty written"<<endl;


	out<<"1 2"<<endl;
//	cout<<"molty's written"<<endl;

	out<<"1 1"<<endl;				//box each atom is in

//	cout<<"Box of each bead written"<<endl;

//	for (x=0; x<atomtypes.size(); x++){
		for (y=0; y<f.size(); y++){
//			if (f.get(y+1).name==atomtypes[x].chemid){
				out<<f.get(y+1).x<<" "<<f.get(y+1).y<<" "<<f.get(y+1).z<<" "<<f.get(y+1).q<<endl;
//			}
		}
//	}

//	cout<<"force field info written"<<endl;

	fout<<out.str();
}
