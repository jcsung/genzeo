#ifndef XYZREADER_H
#define XYZREADER_H

#include "systemreader.h"
#include "chemicalsystem.h"
class XYZReader : public SystemReader
{
	public:
		XYZReader();
		virtual chemicalsystem read(const string &fin);
};
#endif
