
#ifndef ATOMTYPE_H
#define ATOMTYPE_H

#include <string>
#include "atom.h"
#include <iostream>

using namespace std;

struct atomtype{
	int index;
	int type;
	double epsilon;
	double sigma;
	double charge;
	double mass;
	string chemid;
	string chname;

	int count;

	friend ostream &operator<<(ostream &os,const atomtype &at);
	friend istream &operator>>(istream &is,atomtype &at);
	friend bool operator==(atomtype &a1,atomtype &a2);
};
#endif
