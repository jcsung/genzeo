#ifndef PDBREADER_H
#define PDBREADER_H

#include "systemreader.h"
#include "chemicalsystem.h"
class PDBReader : public SystemReader
{
	public:
		PDBReader();
		virtual chemicalsystem read(const string &fin);
};
#endif
